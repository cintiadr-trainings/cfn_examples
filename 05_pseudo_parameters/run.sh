#!/usr/bin/env bash
set -eu

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
BASE="$(basename $DIR)"

. ./scripts/setenv.sh

upload_file $BASE
update_stack my-example-stack 8 true
