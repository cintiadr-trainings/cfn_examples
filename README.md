# Cloudformation training scripts #

Scripts for
<https://slides.com/cintiadelrio/cloudformation/>

Demo done with [demomagic](https://github.com/paxtonhare/demo-magic)


To clean the local changes, run `./clean.sh`

You should run `./<demo>/run.sh` from each folder in numerical order from this directory.
Use `./<demo>/run.sh -n` to skip enter on each line.

All scripts rely on AWS credentials, with write access to the `cfn-sources-cdr` S3 bucket
and Cloudformation permissions.

## Local Setup

  1. Ensure AWS configuration is completed
  1. Install PV
  ```
  brew install pv
  ```
  1. Ensure you have ruby, python and node
  1. Install cfndsl
  ```
  gem install cfndsl
  ```
  1. Install troposphere
  ```
  pip3 install troposphere
  ```
  1. Install serverless
  ```
  npm install -g serverless
  ```
  1. Install cfhighlander
  ```
  gem install cfhighlander
  ```
  1. Install stack_master
  ```
  gem install stack_master
  ```