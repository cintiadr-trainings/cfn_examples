#!/usr/bin/env bash
set -eu

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
BASE="$(basename $DIR)"

. ./scripts/setenv.sh


upload_file $BASE


p "${PURPLE}With EnvironmentType=prod ${COLOR_RESET}"
update_stack my-example-stack 4 true "ParameterKey=EnvironmentType,ParameterValue=prod"

p "${PURPLE}With EnvironmentType=test (default) ${COLOR_RESET}"
update_stack my-example-stack 6 true
