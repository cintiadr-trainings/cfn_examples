## 00
  - tags
  - Uploaded to S3 due to size constraints


## 01
  - delta
  - New resources created
  - Cleanup at the end
  - Docs if replacement or update

## 02
  - Rollback on EC2s
  - Rollback failure!! <-

## 03
  - Intrinsic functions
  - Fn:FindInMap or !FindInMap (You can't nest two instances of two functions in short form.)
  - Capabilities for IAM roles
  - Fn::Transform for template include
  - Macro before running it

  - Moved mappings to includes
  - Ref <---- for Parameters or for resources
  - ssm values for Parameters
  - Condition functions:
    - And
    - Equals
    - If
    - Not
    - Or
  - ALL IN RUNTIME!!!

## 04
  - Includes -> no shorthand notations for YAML snippets
  - Parameters cannot go to includes
  - Exports cannot be changed
    - Fn::ImportValue

## 05
  - No value

## 06
  - Change only parameter


## 07
  - Time to run
  - No changeset!
  - No changes in events

## 08
  - Tools to generate yaml files/templates
    - compile time
    - for/if
    - Parameters vs compiling configuration
  - Modularity ('libraries')


## 09
  - Tools to deploy
    - CDK is also a wrapper
    - serverless is for lambdas