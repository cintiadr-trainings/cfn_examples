#!/usr/bin/env bash
set -eu

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
BASE="$(basename $DIR)"

. ./scripts/setenv.sh

p "cd nested"
upload_file $BASE/nested
create_stack my-nested-stack-root 5

p "${PURPLE}With EnvironmentType=prod${COLOR_RESET}"
update_stack my-nested-stack-root 6 true "ParameterKey=EnvironmentName,ParameterValue=prod"

