#!/usr/bin/env bash
set -eu

########################
# include the magic
########################
. ./scripts/demo-magic.sh


########################
# Configure the options
########################

#
# speed at which to simulate typing. bigger num = faster
#
TYPE_SPEED=100

#
# custom prompt
#
# see http://www.tldp.org/HOWTO/Bash-Prompt-HOWTO/bash-prompt-escape-sequences.html for escape sequences
#
#DEMO_PROMPT="${GREEN}➜ ${CYAN}\w $ "
DEMO_PROMPT="${GREEN}$ "


DATETIME=$(date +%Y%m%d_%H%M%S)


# hide the evidence
clear


#######################
# Helper functions
#######################
upload_file (){
  p "${PURPLE}Uploading template $1 to S3 ${COLOR_RESET}"
  pe "aws s3 cp $1/my-template.yaml s3://cfn-sources-cdr/"

  if [ -f "$1/includes.yaml" ]; then
    pe "aws s3 cp $1/includes.yaml s3://cfn-sources-cdr/"
  fi

  if [ -f "$1/nested-stack.yaml" ]; then
    pe "aws s3 cp $1/nested-stack.yaml s3://cfn-sources-cdr/"
  fi
}

wait_status(){
  STACK_NAME=$1
  FINAL_STATUS="CREATE_COMPLETE UPDATE_COMPLETE DELETE_COMPLETE UPDATE_ROLLBACK_COMPLETE"
  MAGICDEMO="${2:-true}"

  if [[ "$MAGICDEMO" == "true" ]]; then
    p "${CYAN}[Every second]${COLOR_RESET} aws cloudformation describe-stacks --stack-name $STACK_NAME --query 'Stacks[0].StackStatus'"
  fi

  STATUS="INITIAL"
  while ! [[ "$FINAL_STATUS" =~  "$STATUS" ]]; do
    STATUS=$(aws cloudformation describe-stacks --stack-name $STACK_NAME --query 'Stacks[0].StackStatus' --output text)
    echo $STATUS
    sleep 1
  done
}

create_stack (){
  mkdir -p target
  STACK_NAME=$1
  EXPECTED_EVENTS=$2

  p "${PURPLE}Creating stack $STACK_NAME ${COLOR_RESET}"
  pe "aws cloudformation create-stack --stack-name $STACK_NAME \
--template-url https://cfn-sources-cdr.s3-ap-southeast-2.amazonaws.com/my-template.yaml \
--tags Key=Stackname,Value=$STACK_NAME  Key=Name,Value=Cintia"

  wait_status $STACK_NAME

  pe "aws cloudformation describe-stack-events \
--stack-name $STACK_NAME --max-items ${EXPECTED_EVENTS} --output table \
--query 'StackEvents[*].[LogicalResourceId,ResourceStatus,ResourceStatusReason]'"


  aws cloudformation describe-stacks  --stack-name $STACK_NAME --max-items 1 \
   --query "Stacks[0].StackId" --output text > target/stack_id_$STACK_NAME

}

update_stack (){
  STACK_NAME=$1
  EXPECTED_EVENTS=$2
  AUTO_EXPAND=${3:-false}
  PARAMETERS_VALUES=${4:-}

  CAPABILITIES=""
  if [[ "$AUTO_EXPAND" == "true" ]]; then
    CAPABILITIES="--capabilities CAPABILITY_AUTO_EXPAND"
  fi

  PARAMETERS=""
  if [[ "$PARAMETERS_VALUES" != "" ]]; then
    PARAMETERS="--parameters ${PARAMETERS_VALUES}"
  fi

  p "${PURPLE}Updating stack $STACK_NAME ${COLOR_RESET}"
  pe "aws cloudformation update-stack --stack-name $STACK_NAME \
$CAPABILITIES $PARAMETERS \
--template-url  https://cfn-sources-cdr.s3-ap-southeast-2.amazonaws.com/my-template.yaml"

  wait_status $STACK_NAME

pe "aws cloudformation describe-stack-events \
--stack-name $STACK_NAME --max-items $EXPECTED_EVENTS --output table \
--query 'StackEvents[*].[LogicalResourceId,ResourceStatus,ResourceStatusReason]'"

}

create_changeset_stack (){
  STACK_NAME=$1
  EXPECTED_EVENTS=$2
  PARAMETERS_VALUES=$3


  p "${PURPLE}Creating changeset for stack $STACK_NAME ${COLOR_RESET}"

  pe "aws cloudformation create-change-set --stack-name $STACK_NAME \
--use-previous-template --parameters ${PARAMETERS_VALUES} \
--change-set-name ${STACK_NAME}-changeset-1 --query 'Id' --output text | tee target/${STACK_NAME}-changeset-1_id"


  pe "aws cloudformation describe-change-set \
--change-set-name \"$(cat target/${STACK_NAME}-changeset-1_id)\" \
--query 'Changes[*].ResourceChange.[Action,LogicalResourceId,ResourceType,Replacement,Scope[0]]' \
--output table"


  p "${PURPLE}Executing changeset for stack $STACK_NAME ${COLOR_RESET}"

  pe "aws cloudformation execute-change-set \
--change-set-name \"$(cat target/${STACK_NAME}-changeset-1_id)\""

  wait_status $STACK_NAME

  pe "aws cloudformation describe-stack-events \
  --stack-name $STACK_NAME --max-items $EXPECTED_EVENTS --output table \
  --query 'StackEvents[*].[LogicalResourceId,ResourceStatus,ResourceStatusReason]'"

}

delete_stack (){
  STACK_NAME=$1
  aws cloudformation delete-stack --stack-name $STACK_NAME
  set +x
  wait_status "$(cat target/stack_id_$STACK_NAME)" false
  set -x
}
