#!/bin/bash

set -eu

. ./scripts/setenv.sh


set -x

delete_stack my-example-stack

# delete_stack my-nested-stack-root

rm -f target/stack_id_my-example-stack
# rm -f target/stack_id_my-nested-stack-root
