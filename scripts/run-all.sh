#!/bin/bash

set -eu

. ./scripts/setenv.sh

TYPE_SPEED=200

./00_hello_world/run.sh -n
./01_replace_update/run.sh -n
./02_rollback/run.sh -n
./03_parameters_conditions/run.sh -n
./04_outputs_exports/run.sh -n
./05_pseudo_parameters/run.sh -n
./06_changesets/run.sh -n
