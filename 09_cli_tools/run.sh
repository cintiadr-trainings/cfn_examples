#!/bin/bash

set -eu

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
BASE="$(basename $DIR)"

. ./scripts/setenv.sh

cd $BASE
rm -rf serverless/.serverless
rm -rf cfhighlander/out

pe "cd cfhighlander"
pe "cfcompile"

cd ..
pe "cd serverless"
pe "serverless package"

cd ..
pe "cd stack_master"
pe "stack_master compile" 
