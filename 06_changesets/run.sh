#!/usr/bin/env bash
set -eu

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
BASE="$(basename $DIR)"

. ./scripts/setenv.sh

create_changeset_stack my-example-stack 10 "ParameterKey=EnvironmentType,ParameterValue=prod"
