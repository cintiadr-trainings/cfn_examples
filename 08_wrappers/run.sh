#!/usr/bin/env bash
set -eu

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
BASE="$(basename $DIR)"

. ./scripts/setenv.sh

cd $BASE

rm -rf out/*
mkdir -p out/cfndsl
mkdir -p out/troposphere

pe "cd cfndsl"
pe "cfndsl --format yaml my-template.rb -y my-template.config.yaml \
--output ../out/cfndsl/my-template.yaml"

cd ..
pe "cd troposphere"
pe "python3 my-template.py >> ../out/troposphere/my-template.yaml"
