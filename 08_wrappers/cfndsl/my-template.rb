CloudFormation do
  Description "Example template - Just SNS topics"

  # getting info from configuration file
  NUMBER_TOPICS = external_parameters.fetch(:number_topics, '1')

  Parameter(:EnvironmentType) do
    String
    Default "test"
    Description "Environment type"
    AllowedValues ["prod", "test"]
    ConstraintDescription "must specify prod or test"
  end

  Condition(:isOzzie, FnEquals(Ref("AWS::Region"), "ap-southeast-2"))
  Condition(:isProd, FnEquals(Ref(:EnvironmentType), "prod"))

  for i in 0..(NUMBER_TOPICS - 1)
     SNS_Topic("SNSTopic#{i}") do
       DisplayName "typical topic #{i}"
       TopicName "topic#{i}"

       if ( i == 0 )
         add_tag('EnvironmentId', FnSub("${AWS::Region}:${AWS::AccountId}:${EnvironmentType}"))
         add_tag('Ozzie', FnIf(:isOzzie, "Oi oi oi", "Yeah nah"))
         add_tag('isProd', FnJoin('|', [:isProd, "pppp", Ref("AWS::NoValue")]))
       elsif ( i == 2)
         Condition(:isProd)
       end
     end

     if ( i == 0 )
        Output("SNSRef#{i}") do
          Value Ref("SNSTopic#{i}")
          Description "Ref for SNS#{i}"
          Export(FnJoin("-", [
            Ref(:EnvironmentType), "sns#{i}"
           ]))
        end

        Output("SNSGetAttr#{i}") do
          Value FnGetAtt("SNSTopic#{i}", "TopicName")
          Description "Topic name for SNS#{i}"
        end
     end
  end
end
