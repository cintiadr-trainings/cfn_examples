#!/usr/bin/env node
import cdk = require('@aws-cdk/core');
import sns = require('@aws-cdk/aws-sns');

const NUMBER_TOPICS = 3

class MyTemplateStack extends cdk.Stack {
  constructor(app: cdk.App, id: string) {
    super(app, id);

    var i:number;
    for(i = 0;i <=NUMBER_TOPICS - 1;i++) {
       new sns.Topic(this, `SNSTopic${i}`, {
          topicName: `Topic ${i}`,
          displayName: 'Customer subscription topic '
      });
    }
  }
}

const app = new cdk.App();
new MyTemplateStack(app, 'MyTemplateStack');
// app.synth();
