from troposphere import Template, Parameter, Output, Tags
from troposphere import Ref, GetAtt, Join, Output, Equals, If
import troposphere.sns as sns

t = Template()

t.add_parameter(Parameter(
    "EnvironmentType",
    Description="Environment type",
    Type="String",
    AllowedValues=["prod", "test"],
    Default="test",
    ConstraintDescription="must specify prod or test",
))

is_ozzie = t.add_condition("isOzzie", Equals(Ref("AWS::Region"), "ap-southeast-2"))
is_prod = t.add_condition("isProd", Equals(Ref("EnvironmentType"), "prod"))

for i in range(0, 3):
    topic = sns.Topic(
      "SNSTopic%d" % i,
        DisplayName=("typical topic %d" % i)
    )

    tags = Ref("AWS:NoValue")
    if ( i == 0 ):
        tags = Tags(foo='foo')
    topic.Tags = tags

    if ( i == 2 ):
        topic.Condition = is_prod

    t.add_resource(topic)

    if ( i == 0 ):
        t.add_output([
            Output(
                ("SNSRef%d" % i),
                Description=("Ref for SNS %d" % i),
                Value=Ref(topic),
            ),
            Output(
                ("SNSGetAttr%d" % i),
                Description=("Topic name for SNS %d" % i),
                Value=GetAtt(topic, "TopicName"),
            )
        ])

print(t.to_yaml())
